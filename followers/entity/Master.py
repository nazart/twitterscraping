import entity.model.TwitterMaster as TwitterMaster
import entity.model.Db as Db

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime
from datetime import timedelta

class Master():
	name = ''
	description = ''
	idMaster = 0
	status = 0
	daysMaster = 7
	twitterMasterModel = None

	def __init__(self):
		self.session = Db.getInstance().session
		twitterMasterModel = None

	def identifyById(self,idMaster):
		self.twitterMasterModel = self.session.query(TwitterMaster).filter(TwitterMaster.i_tmast_id==idMaster).first()
		if(self.twitterMasterModel!=None):
			self.name = self.twitterMasterModel.v_tmast_user_name
			self.description = self.twitterMasterModel.v_tmast_description
			self.idMaster = self.twitterMasterModel.i_tmast_id
			self.status = self.twitterMasterModel.i_tmast_status

	def save(self):
		if(self.twitterMasterModel != None):
			self.twitterMasterModel.v_tmast_user_name = self.name
			self.twitterMasterModel.v_tmast_description = self.description
			self.twitterMasterModel.i_tmast_status = self.status
		else:
			self.twitterMasterModel = TwitterMaster(self.name, self.description, self.status)

		self.session.add(self.twitterMasterModel)
		self.session.commit()
		self.idMaster = self.twitterMasterModel.i_tmast_id

	@staticmethod
	def getAllMasterActive():
		return Db.getInstance().session.query(TwitterMaster).filter(TwitterMaster.i_tmast_status==1).all()