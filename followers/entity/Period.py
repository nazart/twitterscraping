import entity.model.TwitterPeriod as TwitterPeriod
import entity.model.Db as Db

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime
from datetime import timedelta

class Period():
	start = ''
	end = ''
	idPeriod = 0
	status = 0
	daysPeriod = 7
	twitterPeriodModel = None

	def __init__(self):
		self.session = Db.getInstance().session
		self.twitterPeriodModel = None

	def identifyById(self,idPeriod):
		self.twitterPeriodModel = self.session.query(TwitterPeriod).filter(TwitterPeriod.i_tper_id==idPeriod).first()
		if(self.twitterPeriodModel!=None):
			self.start = self.twitterPeriodModel.d_tper_ini_date
			self.end = self.twitterPeriodModel.d_tper_end_date
			self.idPeriod = self.twitterPeriodModel.i_tper_id
			self.status = self.twitterPeriodModel.i_tper_status

	def save(self):
		if(self.twitterPeriodModel != None):
			self.twitterPeriodModel.d_tper_ini_date = self.start
			self.twitterPeriodModel.d_tper_end_date = self.end
			self.twitterPeriodModel.i_tper_status = self.status
		else:
			self.twitterPeriodModel = TwitterPeriod(self.start, self.end, self.status)

		self.session.add(self.twitterPeriodModel)
		self.session.commit()
		self.idPeriod = self.twitterPeriodModel.i_tper_id

	@staticmethod
	def startPeriod():
		LastPeriod = Period()
		LastPeriod.identifyLastPeriod()
		if (LastPeriod.twitterPeriodModel!=None):
			LastPeriod.status = 0
			LastPeriod.save()
		NewPeriod = Period()
		NewPeriod.start = datetime.now(tz=None)
		NewPeriod.end = NewPeriod.start + timedelta(days=LastPeriod.daysPeriod)
		NewPeriod.status = 1
		NewPeriod.save()
		return NewPeriod

	def identifyLastPeriod(self):
		self.twitterPeriodModel = self.session.query(TwitterPeriod).filter(TwitterPeriod.i_tper_status==1).first()
		if(self.twitterPeriodModel!=None):
			self.start = self.twitterPeriodModel.d_tper_ini_date
			self.end = self.twitterPeriodModel.d_tper_end_date
			self.idPeriod = self.twitterPeriodModel.i_tper_id
			self.status = self.twitterPeriodModel.i_tper_status