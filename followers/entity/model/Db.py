from sqlalchemy import create_engine
import json
from sqlalchemy.orm import sessionmaker

class Db():
	# Here will be the instance stored.
	__instance = None
	session = ''

	@staticmethod
	def getInstance():
		if (Db.__instance == None) :
			Db()
		return Db.__instance

	def __init__(self):
		if (Db.__instance == None) :
			engine = create_engine("mysql+pymysql://barbarianuser:password@192.168.1.32/octopus",encoding='utf-8', echo=True)
			Session = sessionmaker(bind=engine)
			self.session = Session()
			Db.__instance = self
		else:
			Db.__instance = self