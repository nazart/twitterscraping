from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import func

Base = declarative_base()

class TwitterMaster(Base):
	__tablename__ = "tb_twitter_master"
	i_tmast_id = Column(Integer, primary_key=True)
	v_tmast_user_name = Column(String)
	v_tmast_description = Column(String)
	i_tmast_status = Column(Integer)
	date_cr = Column(DateTime(timezone=True), server_default=func.now())
	user_cr = Column(Integer)
	pc_cr = Column(String)
	date_up = Column(DateTime(timezone=True), onupdate=func.now())
	user_up = Column(Integer)
	pc_up = Column(String)

	def __init__(self, name, description, status):
		self.v_tmast_user_name = name
		self.v_tmast_description = description
		self.i_tmast_status = status
		self.user_cr = 0
		self.pc_cr = '127.0.0.1'