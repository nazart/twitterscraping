from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import func

Base = declarative_base()

class TwitterUser(Base):
	__tablename__ = "tb_twitter_user"
	i_user_id = Column(Integer, primary_key=True)
	v_twitter_user_id = Column(String)
	v_user_name = Column(String)
	v_user_screen_name = Column(String)
	v_user_location = Column(String)
	v_user_lang = Column(String)
	i_user_follower_count = Column(Integer)
	v_user_friend_count = Column(Integer)
	date_up = Column(DateTime(timezone=True), onupdate=func.now())
	date_cr = Column(DateTime(timezone=True), server_default=func.now())
	user_cr = Column(Integer)
	pc_cr = Column(String)

	def __init__(self, idTwitter, name, screenName, location,lang,followers,friends):
		self.v_twitter_user_id = idTwitter
		self.v_user_name = name
		self.v_user_screen_name = screenName
		self.v_user_location = location
		self.v_user_lang = lang
		self.i_user_follower_count = followers
		self.v_user_friend_count = friends
		self.user_cr = 0
		self.pc_cr = '127.0.0.1'