from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import func

Base = declarative_base()

class TwitterCredential(Base):
	__tablename__ = "tb_twitter_credential"
	i_tcre_id = Column(Integer, primary_key=True)
	v_tcre_account = Column(String)
	v_tcre_access_token = Column(String)
	v_tcre_access_token_secret = Column(String)
	v_tcre_consumer_key = Column(String)
	v_tcre_consumer_secret = Column(String)
	i_tcre_status = Column(Integer)
	date_cr = Column(DateTime(timezone=True), server_default=func.now())
	user_cr = Column(Integer)
	pc_cr = Column(String)
	date_up = Column(DateTime(timezone=True), onupdate=func.now())
	user_up = Column(Integer)
	pc_up = Column(String)

	def __init__(self, account, token, tokenSecret, customerKey, customeSecret,status):
		self.v_tcre_account = account
		self.v_tcre_access_token = token
		self.v_tcre_access_token_secret = tokenSecret
		self.v_tcre_consumer_key = customerKey
		self.v_tcre_consumer_secret = customeSecret
		self.i_tcre_status = status
		self.user_cr = 0
		self.pc_cr = '127.0.0.1'