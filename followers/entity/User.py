import entity.model.TwitterUser as TwitterUser
import entity.model.Db as Db

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

class User:
	screenName = ''
	location = ''
	lang = ''
	followers = ''
	friends = 0
	name = ''
	idTwitter = ''
	idUser = 0

	twitterUserEntity = None

	def __init__(self):
		self.session = Db.getInstance().session
		self.twitterUserEntity = None

	def identifyForTwitterId(self,twitterId):
		self.twitterUserEntity = self.session.query(TwitterUser).filter(TwitterUser.v_twitter_user_id==twitterId).first()
		if(self.twitterUserEntity!=None):
			self.idUser = self.twitterUserEntity.i_user_id
			self.screenName = self.twitterUserEntity.v_user_screen_name
			self.location = self.twitterUserEntity.v_user_location
			self.lang = self.twitterUserEntity.v_user_lang
			self.followers = self.twitterUserEntity.i_user_follower_count
			self.friends = self.twitterUserEntity.v_user_friend_count
			self.name = self.twitterUserEntity.v_user_name
			self.idTwitter = self.twitterUserEntity.v_twitter_user_id

	def save(self):
		if(self.idUser > 0 and self.twitterUserEntity == None):
			self.twitterUserEntity = self.session.query(TwitterUser).filter(TwitterUser.i_user_id==self.idUser).first()
		if(self.twitterUserEntity != None):
			self.twitterUserEntity.v_user_screen_name = self.screenName
			self.twitterUserEntity.v_user_location = self.location
			self.twitterUserEntity.v_user_lang = self.lang
			self.twitterUserEntity.i_user_follower_count = self.followers
			self.twitterUserEntity.v_user_friend_count = self.friends
			self.twitterUserEntity.v_user_name = self.name
			self.twitterUserEntity.v_twitter_user_id = self.idTwitter
		else:
			self.twitterUserEntity = TwitterUser(self.idTwitter, self.name, self.screenName, self.location, self.lang, self.followers, self.friends)
		self.session.add(self.twitterUserEntity)
		self.session.commit()
		self.idUser = self.twitterUserEntity.i_user_id

	def setUserForApiTwitter(self,userTwitter):
		#pregunta si es que el userTwitterExiste y ademas si es que tiene el atributo id -> hasattr()
		if(userTwitter != None and hasattr(userTwitter, 'id')):
			self.identifyForTwitterId(userTwitter.id)
			self.screenName = userTwitter.screen_name
			self.location = userTwitter.location
			self.lang = userTwitter.lang
			self.followers = userTwitter.followers_count
			self.friends = userTwitter.friends_count
			self.name = userTwitter.name
			self.idTwitter = userTwitter.id
			self.save()
