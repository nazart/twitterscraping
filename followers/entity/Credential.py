import entity.model.TwitterCredential as TwitterCredential
import entity.model.Db as Db

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime
from datetime import timedelta

class Credential():
	account = ''
	token = ''
	tokenSecret = ''
	consumerKey = ''
	consumerSecret = ''
	idCredential = 0
	status = 0
	twitterCredentialModel = None

	def __init__(self):
		self.session = Db.getInstance().session
		twitterCredentialModel = None

	def identifyById(self,idCredential):
		self.twitterCredentialModel = self.session.query(TwitterCredential).filter(TwitterCredential.i_tcre_id==idCredential).first()
		if(self.twitterCredentialModel!=None):
			self.account = self.twitterCredentialModel.v_tcre_account
			self.token = self.twitterCredentialModel.v_tcre_access_token
			self.tokenSecret = self.twitterCredentialModel.v_tcre_access_token_secret
			self.consumerKey = self.twitterCredentialModel.v_tcre_consumer_key
			self.consumerSecret = self.twitterCredentialModel.v_tcre_consumer_secret
			self.idCredential = self.twitterCredentialModel.i_tcre_id
			self.status = self.twitterCredentialModel.i_tcre_status

	def save(self):
		if(self.twitterCredentialModel != None):
			self.twitterCredentialModel.v_tcre_account = self.account
			self.twitterCredentialModel.v_tcre_access_token = self.token
			self.twitterCredentialModel.v_tcre_access_token_secret = self.tokenSecret
			self.twitterCredentialModel.v_tcre_consumer_key = self.consumerKey
			self.twitterCredentialModel.v_tcre_consumer_secret = self.consumerSecret
			self.twitterCredentialModel.i_tcre_status = self.status
		else:
			self.twitterCredentialModel = TwitterCredential(self.account, self.token, self.tokenSecret, self.consumerKey, self.consumerSecret, self.status)

		self.session.add(self.twitterCredentialModel)
		self.session.commit()
		self.idCredential = self.twitterCredentialModel.i_tcre_id

	@staticmethod
	def getAllCredentialActive():
		return Db.getInstance().session.query(TwitterCredential).filter(TwitterCredential.i_tcre_status==1).all()