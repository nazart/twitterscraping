#Import the necessary methods from tweepy library
import tweepy
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import os
import json
import time
from datetime import datetime
import sys
from entity import Period
from entity import Master
from entity import Credential
import pathlib
import numpy as numpy
from entity import User
import uuid
import threading

twitter_handle='Politica_ECpe'

#lista de conecciones que tendra con el api de twitter
apiConecctions = []
#lista de credenciales
listCredentials = []
def getApiConecction(twitterConecction):
	idCredential = twitterConecction.i_tcre_id
	print('credencial numero ', idCredential)
	print(listCredentials)
	first = False
	try:
		indice = listCredentials.index(idCredential)
	except Exception as e:
		first = True
		indice = 0
	if(first):
		access_token = twitterConecction.v_tcre_access_token
		access_token_secret = twitterConecction.v_tcre_access_token_secret
		consumer_key = twitterConecction.v_tcre_consumer_key
		consumer_secret = twitterConecction.v_tcre_consumer_secret
		auth = OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		listCredentials.append(idCredential)
		api = tweepy.API(auth)
		apiConecctions.append(api)
		return api
	else:
		return apiConecctions[indice]
		

class masterConecction():
	masterModel = None
	twitterConecction = None

	def __init__(self,masterModel,twitterConecction):
		self.masterModel = masterModel
		self.twitterConecction = getApiConecction(twitterConecction)
		#self.apiConecction = apiConecction

def processReadTwitter(masterConecctionInstance,entityPeriod,pathPeriod,UserMasterObject):
	api = masterConecctionInstance.twitterConecction
	masterModel = masterConecctionInstance.masterModel
	pathMaster = pathPeriod + '/' + str(UserMasterObject.idUser)
	if not os.path.isdir(pathMaster):
		os.makedirs(pathMaster)
	# retorna las paginas de los followers para luego ser ingresadas en un file
	pages = tweepy.Cursor(api.followers, screen_name=UserMasterObject.screenName).pages()
	print(pages)
	sleeptime = 4
	while True:
		print('inicia procesamiento:' +masterModel.v_tmast_user_name)
		saveFile = True
		try:
			page = next(pages)
			time.sleep(sleeptime)
			saveFile = True
		except tweepy.TweepError: #taking extra care of the "rate limit exceeded"
			segundos=(1*60)
			print('esperando ',segundos,' segundos')
			suma = segundos
			while (suma>0):
				print ("faltan segundos:" ,suma, end='\r')
				suma=(suma-1)
				time.sleep(1)
			saveFile = False
		if(saveFile):
			nameFile = pathMaster+'/'+str(uuid.uuid4())+'.json'
			with open(nameFile, 'a') as f:
				for user in page:
					f.write(json.dumps(user._json))
					f.write('\n')
	#empieza el proceso de lectura de los followers de


	#print(json.dumps(a._json))

def getPeriod():
	# crea periodos de manera automatica y desactiva el ultimo periodo registrado
	return Period.startPeriod()

entityPeriod = getPeriod()
starDate = entityPeriod.start
endDate = entityPeriod.end

while True:
	# una vez que un periodo haya terminado empieza a ejecutarse los demas elementos
	if(endDate < datetime.now(tz=None)):
		entityPeriod = getPeriod()
		starDate = entityPeriod.start
		endDate = entityPeriod.end 
	else:
		pathPeriod = 'resource/followersjson/period-'+str(entityPeriod.idPeriod)
		#verifica de que exista la ruta si no existe la crea
		if not os.path.isdir(pathPeriod):
			os.makedirs(pathPeriod)
			#crea los periodos una vez que haya caducado uno de los periodos
			print('se ha cereado la carpeta del periodo :' +pathPeriod)
			listMasters = Master.getAllMasterActive()
			listCredentialsTwitter = Credential.getAllCredentialActive()
			cantCredential = len(listCredentialsTwitter)
			listMastersForCredential = numpy.array_split(listMasters,cantCredential)
			#print(listMastersForCredential)
			listMasterConecction = []
			for index,listMaster in enumerate(listMastersForCredential, start=0):
				for master in listMaster:
					mascConec = masterConecction(master,listCredentialsTwitter[index])
					listMasterConecction.append(mascConec)
			for masterConecction in listMasterConecction:
				api = masterConecction.twitterConecction
				masterModel = masterConecction.masterModel
				userApi = api.get_user(masterModel.v_tmast_user_name)
				UserMasterObject = User()
				UserMasterObject.setUserForApiTwitter(userApi)
				t = threading.Thread(target=processReadTwitter,args=(masterConecction,entityPeriod,pathPeriod,UserMasterObject))
				print('procesa: ' + str(masterModel.v_tmast_user_name))
				t.start()
				#processReadTwitter(masterConecction,entityPeriod,pathPeriod)
			#print(listMasterConecction)
			exit()
			#for master in listMasters
			#	pathMaster = pathPeriod + '/master-' + master.i_tmast_id
			#	if not os.path.isdir(pathMaster):
			#		os.makedirs(pathMaster)

	pass
exit()
#Variables that contains the user credentials to access Twitter API 






users = tweepy.Cursor(api.followers, screen_name=twitter_handle).items()
#este codigo permite obtener los datos del handle
a = api.get_user(twitter_handle)
print(json.dumps(a._json))
exit()

while True:
	with open('output.json', 'a') as f:
		try:
			user = users.next()
			f.write(json.dumps(user._json))
			f.write('\n')
		except:
			segundos=(1*60)
			print('esperando ',segundos,' segundos')
			suma = segundos
			while (suma>0):
				print ("faltan segundos:" ,suma, end='\r')
				suma=(suma-1)
				time.sleep(1)



#with open('output2.json', 'a') as f:
#    # api.GetStreamFilter will return a generator that yields one status
#    # message (i.e., Tweet) at a time as a JSON dictionary.
#	try:
#		for follower in tweepy.Cursor(api.followers, screen_name=twitter_handle).items():
#			f.write(json.dumps(follower._json))
#			f.write('\n')
#	except:
#		print('esperando 5 minutos')
#		time.sleep(5*60)